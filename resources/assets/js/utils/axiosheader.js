import axios from 'axios';

const setAxiosToken = (token) =>{
    axios.defaults.headers.common['Authorization'] = `Bearer ${token}`;
}

export default setAxiosToken;