import LoginComponent from '../components/LoginComponent.vue';
import RegisterComponent from '../components/RegisterComponent.vue';
import DashboardComponent from '../components/DashboardComponent';

const routes = [
    {
        name: 'login',
        path: '/',
        component: LoginComponent
    },
    {
        name: 'register',
        path: '/register',
        component: RegisterComponent
    },
    {
        name: 'user',
        path: '/dashboard',
        component: DashboardComponent
    },
];


export default routes;