<?php

namespace App;

use Illuminate\Database\Eloquent\Relations\Pivot;

class DomainUser extends Pivot
{
    protected $table = 'domain_user';
    public $timestamps = false;

    /**
     *  The attribute that help to confirm domain's owner
     * */
    protected $fillable = ['domain_id','user_id','confirmation_code'];

}
