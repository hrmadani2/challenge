<?php

namespace App;

use App\Events\CreatedDomain;
use App\Events\DeletedDomain;
use App\Scopes\DomainOwnerScope;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;

class Domain extends Model
{
    protected $fillable = ['domain'];

    protected $appends = ['confirmation'];

    /**
     * Events of the model
     *
     * @var array
     * */
    protected $dispatchesEvents = [
        'created' => CreatedDomain::class,
        'deleted' => DeletedDomain::class
    ];

    /**
     * The "booting" method of the model.
     *
     * @return void
     */
    protected static function boot()
    {
        parent::boot();

        static::addGlobalScope(new DomainOwnerScope);
    }

    /**
     * The users that belong to domain
     *
     * */
    public function users()
    {
        return $this->belongsToMany('App\User')->withPivot('confirmed_at', 'confirmation_code');
    }

    /**
     * Add Confirmation Key to retrieve data from domains
     *
     * @return array
     * */
    public function getConfirmationAttribute()
    {
        $domainConfirmation = DomainUser::where([
            'user_id' => Auth::guard('api')->id(),
            'domain_id' => $this->attributes['id']
        ])->get()->toArray();

        return (count($domainConfirmation) > 0)? $domainConfirmation[0] : $domainConfirmation;
    }

}
