<?php

namespace App\Http\Controllers\API\v1;

use App\Domain;
use App\DomainUser;
use App\Events\CreatedDomain;
use App\Events\DeletedDomain;
use App\Events\UpdateDomainUser;
use App\Scopes\DomainOwnerScope;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;


class DomainController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return response()->json(['data' => Domain::orderBy('id','desc')->get()->toArray()], 200);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'domain' => 'required | url'
        ]);

        //Only store protocol://subdomain.domain.tld:port
        preg_match('#^(?:\w+://)?([^/]+)#i', $request->input('domain'), $cleanDomain);

        $domain = Domain::where('domain', $cleanDomain[0])->withoutGlobalScope(DomainOwnerScope::class)->first();
        //Domain was stored in DB or Not
        if ($domain instanceof Domain) {
            event(new CreatedDomain($domain));
        } else {
            $domain = Domain::create([
                'domain' => $cleanDomain[0]
            ]);
        }

        return response()->json([
            'data' => $domain,
        ], 201);

    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show(Domain $domain)
    {
        return response()->json(['data' => $domain], 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function delete(Domain $domain)
    {
        if ($domain->users->count() > 1) {
            event(new DeletedDomain($domain));
        } else {
            $domain->delete();
        }

        return response()->json(['data' => 'deleted successfully'], 204);
    }

}
