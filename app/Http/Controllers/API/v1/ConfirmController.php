<?php

namespace App\Http\Controllers\API\v1;

use App\Domain;
use App\DomainUser;
use App\Events\UpdateDomainUser;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;

class ConfirmController extends Controller
{
    /**
     * Confirm the domain
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function ConfirmDomain(Domain $domain, $type = 'txt')
    {

        if ($type === 'txt') {

            //Generate the url link
            $url = $domain['domain'] . "/confirm.txt";

            //Get confirmation Code from Destination
            $result = $this->getconfirmCodeFromDestination($url);

            //URL doesn't exist
            if (!$result) {
                return \response()->json(['data' => "Confirmation Failed. URL doesn't find."], 410);
            }

            //Check Confirmation Code
            if ($result !== $domain['confirmation']['confirmation_code']) {
                return \response()->json(['data' => "Confirmation Failed. Code is incorrect."], 410);
            }

            //Update
            DomainUser::where([
                'domain_id' => $domain['id'],
                'user_id' => Auth::guard('api')->id()
            ])->update(['confirmed_at' => now()]);

            //Instance of Pivot Table
            $domainUser = DomainUser::where([
                'domain_id' => $domain['id'],
                'user_id' => Auth::guard('api')->id()
            ])->first();
            //Policy : Only One User-Domain can be confirmed
            event(new UpdateDomainUser($domainUser));

            return \response()->json(['data' => "Domain is confirmed successfully."], 200);
        }
    }

    /**
     * Read confirmation code from Destination
     *
     * @param $url
     * @return false | string
     * */
    public function getconfirmCodeFromDestination($url)
    {
        set_time_limit(10);
        try {

            //Handle SSL verification
            $arrContextOptions = array(
                "ssl" => array(
                    "verify_peer" => false,
                    "verify_peer_name" => false,
                ),
                'http' => array(
                    'method' => "GET",
                    'header' => "Accept-language: en\r\n"
                )
            );

            $result = file_get_contents($url, false, stream_context_create($arrContextOptions), 0, 50);
        } catch (\Exception $e) {
            $result = false;
        } finally {
            return $result;
        }
    }
}
