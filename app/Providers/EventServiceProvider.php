<?php

namespace App\Providers;

use Illuminate\Support\Facades\Event;
use Illuminate\Foundation\Support\Providers\EventServiceProvider as ServiceProvider;

class EventServiceProvider extends ServiceProvider
{
    /**
     * The event listener mappings for the application.
     *
     * @var array
     */
    protected $listen = [
        'App\Events\CreatedDomain' => [
            'App\Listeners\AddDomainUserPivotInformation',
        ],
        'App\Events\DeletedDomain' => [
            'App\Listeners\RemoveDomainUserPivotInformation',
        ],
        'App\Events\UpdateDomainUser' => [
            'App\Listeners\UpdateDomainUserPivotInformation',
        ],
    ];

    /**
     * Register any events for your application.
     *
     * @return void
     */
    public function boot()
    {
        parent::boot();

        //
    }
}
