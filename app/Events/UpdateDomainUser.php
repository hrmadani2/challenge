<?php

namespace App\Events;

use App\DomainUser;
use Illuminate\Queue\SerializesModels;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Broadcasting\InteractsWithSockets;

class UpdateDomainUser
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    public $domainUser;
    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct(DomainUser $domainUser)
    {
        $this->domainUser = $domainUser;
    }

}
