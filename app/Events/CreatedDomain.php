<?php

namespace App\Events;

use App\Domain;
use Illuminate\Queue\SerializesModels;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Broadcasting\InteractsWithSockets;

class CreatedDomain
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    public $domain;
    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct(Domain $domain)
    {
        $this->domain = $domain;
    }
}
