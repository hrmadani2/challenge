<?php

namespace App\Listeners;

use App\DomainUser;
use App\Events\DeletedDomain;
use Illuminate\Support\Facades\Auth;

class RemoveDomainUserPivotInformation
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  DeletedDomain  $event
     * @return void
     */
    public function handle(DeletedDomain $event)
    {
        DomainUser::where([
            'domain_id' => $event->domain->id,
            'user_id' => Auth::guard('api')->id()
        ])->delete();
    }
}
