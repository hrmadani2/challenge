<?php

namespace App\Listeners;

use App\DomainUser;
use App\Events\UpdateDomainUser;
use Illuminate\Support\Facades\Auth;

class UpdateDomainUserPivotInformation
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  UpdateDomainUser  $event
     * @return void
     */
    public function handle(UpdateDomainUser $event)
    {
        DomainUser::where([
            ["domain_id" , "=" , $event->domainUser->domain_id],
            ['user_id', '<>' , Auth::guard('api')->id()]
        ])->update(['confirmed_at'=> null]);
    }
}
