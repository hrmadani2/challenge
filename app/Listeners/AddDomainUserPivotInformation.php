<?php

namespace App\Listeners;

use App\DomainUser;
use App\Events\CreatedDomain;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Str;

class AddDomainUserPivotInformation
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  CreatedDomain  $event
     * @return void
     */
    public function handle(CreatedDomain $event)
    {
        DomainUser::firstOrCreate(
            [
                'domain_id'  => $event->domain->id,
                'user_id' => Auth::guard('api')->id()
            ],
            [
                'confirmation_code' => Str::random(50)
            ]);
    }
}