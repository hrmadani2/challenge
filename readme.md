# HRMadani Challenge

## Technologies
*  PHP
*  Laravel 5.5
*  VueJs
*  Bootstrap

## Login Page
![Screenshot_2019-10-21_HRMadani_Challenge](/uploads/1faabd787a6f9e7ac19c67d516e8ca85/Screenshot_2019-10-21_HRMadani_Challenge.png)
## Register Page
![Screenshot_2019-10-21_HRMadani_Challenge_1_](/uploads/ef0b39078098bc4be430ea4896079c25/Screenshot_2019-10-21_HRMadani_Challenge_1_.png)
## Dashboard
![Screenshot_2019-10-21_HRMadani_Challenge_2_](/uploads/0d95846bac56b5b1ac72bcf8e31fa6f9/Screenshot_2019-10-21_HRMadani_Challenge_2_.png)

## Deployment on LAMP
`cd /opt/project_name`

`cp .env.example .env`

`php artisan key:generate`

`‫‪chmod‬‬ ‫‪-R‬‬ ‫‪700‬‬ ‫‪/opt/project_name/vendor‬‬`

`‫‪chmod‬‬ ‫‪-R‬‬ ‫‪700‬‬ ‫‪/opt/project_name/storage‬‬`

`‫‪chmod‬‬ ‫‪-R‬‬ ‫‪700‬‬ ‫‪/opt/project_name/public/index.php‬‬`

`‫‪nano‬‬ ‫‪.env‬‬`

## Change These Items

```
‫‪APP_URL=https://example.com

‫‪DB_DATABASE=DB_NAME‬‬

‫‪DB_USERNAME=DB_USERNAME‬‬

‫‪DB_PASSWORD=DB_PASSWORD‬‬
```

`‫‪php‬‬ ‫‪artisan‬‬ ‫‪migrate‬`

`php artisan db:seed`

## Run Feature Tests

`composer test`

## Default User

Username : admin

Password : password