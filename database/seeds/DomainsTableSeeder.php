<?php

use Illuminate\Database\Seeder;

class DomainsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //disable foreign key check for this connection before running seeders
        \Illuminate\Support\Facades\DB::statement('SET FOREIGN_KEY_CHECKS=0;');

        //Preventing Event
        \App\Domain::flushEventListeners();

        //Clean up the table
        \App\Domain::truncate();

        //Make factory
        $faker = \Faker\Factory::create();

        for ($i = 0; $i < 20; $i++) {

            //Only store protocol://subdomain.domain.tld
            preg_match('#^(?:\w+://)?([^/]+)#i', $faker->unique()->url, $cleanDomain);

            if (\App\Domain::where('domain', $cleanDomain[0])->count() == 0) {
                $domain = \App\Domain::create([
                    'domain' => $cleanDomain[0],
                ])->withoutGlobalScopes()  ;
            }


        }
    }
}
