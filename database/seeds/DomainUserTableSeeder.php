<?php

use Illuminate\Database\Seeder;

class DomainUserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //disable foreign key check for this connection before running seeders
        \Illuminate\Support\Facades\DB::statement('SET FOREIGN_KEY_CHECKS=0;');

        \App\DomainUser::truncate();

        for ($i = 0; $i < 30; $i++) {
            \App\DomainUser::create([
                'domain_id' => rand(1, 20),
                'user_id' => rand(1,10),
                'confirmation_code' => \Illuminate\Support\Str::random(50)
            ]);
        }
    }
}
