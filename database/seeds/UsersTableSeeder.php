<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //disable foreign key check for this connection before running seeders
        DB::statement('SET FOREIGN_KEY_CHECKS=0;');

        //Clean up the table
        \App\User::truncate();

        //Make instance from factory
        $faker = \Faker\Factory::create();

        //All user have the same password in test
        $pass = bcrypt("password");

        //Create Admin user
        \App\User::create([
            'name' => 'admin',
            'email' => 'admin@test.com',
            'password' => $pass
        ]);

        //Other users for test
        for ($i=0; $i<10 ; $i++){
            \App\User::create([
                'name' => $faker->name,
                'email' => $faker->email,
                'password' => $pass
            ]);
        }
    }
}
