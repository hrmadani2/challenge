<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */
use App\Domain;
use Faker\Generator as Faker;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(Domain::class, function (Faker $faker) {
    //Only store protocol://subdomain.domain.tld
    preg_match('#^(?:\w+://)?([^/]+)#i', $faker->unique()->url, $cleanDomain);
    return [
        'domain' => $cleanDomain[0],
    ];
});
