<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */
use App\DomainUser;
use Illuminate\Support\Str;
use Faker\Generator as Faker;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(DomainUser::class, function (Faker $faker) {
    return [
        'user_id' => factory(\App\User::class)->create()->id,
        'domain_id' => factory(\App\Domain::class)->create()->id,
        'confirmed_at' => now(),
        'confirmation_code' => Str::random(50),
    ];
});
