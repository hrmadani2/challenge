<?php

namespace Tests\Feature;

use Tests\TestCase;

class LoginTest extends TestCase
{
    /**
     * User Login Test
     * Test Requirements for login
     *
     * @return void
     */
    public function testLoginRequires()
    {
        $headers = [
            'Accept' => "application/json",
            'Content-Type' => "application/json",
        ];

        $this->json('POST', 'api/login' , [] , $headers)
            ->assertStatus(422);
    }

    /**
     * User Login Test
     * Test Login Successfully
     *
     * @return void
     */
    public function testLoginSuccessfully()
    {
        $headers = [
            'Accept' => "application/json",
            'Content-Type' => "application/json",
        ];

        $payLoad = ['email' => 'admin@test.com', 'password' => 'password'];

        $this->json('POST', 'api/login', $payLoad , $headers)
            ->assertStatus(201)
            ->assertJsonStructure([
                "data" => [
                    "id",
                    "name",
                    "email",
                    "api_token",
                    "created_at",
                    "updated_at"
                ]
            ]);
    }

    /**
     * User Login Test
     * Test Login Failed
     *
     * @return void
     */
    public function testLoginFail()
    {
        $headers = [
            'Accept' => "application/json",
            'Content-Type' => "application/json",
        ];

        $payLoad = ['email' => 'admin@test.com', 'password' => '1234'];

        $this->json('POST', 'api/login', $payLoad , $headers)
            ->assertStatus(422)
            ->assertJsonStructure([
                "message",
                'errors'
            ]);
    }
}
