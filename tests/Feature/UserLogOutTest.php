<?php

namespace Tests\Feature;

use App\User;
use Tests\TestCase;

class UserLogOutTest extends TestCase
{
    /**
     * Test User Logged Out Successfully
     *
     * @return void
     */
    public function testUserLoggedOutSuccessfully()
    {
        //Simulating Login
        $user = factory(User::class)->create(['email' => 'user@test.com']);
        $token = $user->generateApiToken();
        $headers = [
            'Accept' => "application/json",
            'Content-Type' => "application/json",
            'Authorization' => "Bearer $token"
        ];

        $this->json('get', '/api/logout', [], $headers)
            ->assertStatus(200);

        $user = User::find($user->id);

        $this->assertEquals(null, $user->api_token);
    }
}
