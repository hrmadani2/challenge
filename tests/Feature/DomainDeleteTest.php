<?php

namespace Tests\Feature;

use App\Domain;
use App\DomainUser;
use App\User;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Str;
use Tests\TestCase;

class DomainDeleteTest extends TestCase
{
    /**
     * Delete An Exist Domain that related to logged in users
     *
     * @return void
     */
    public function testDeleteDomainSuccessfully()
    {
        //Simulating : User login
        $user = factory(User::class)->create(['email' => 'user@testDeleteDomainSuccessfully.com']);
        $token = $user->generateApiToken();

        //create a domain
        $domain = factory(Domain::class)->create(['domain' => 'https://testDeleteDomainSuccessfully.com']);

        //Join Domain and User together
        $domainUser = factory(DomainUser::class)->create([
            'user_id' => $user->id,
            'domain_id' => $domain->id,
        ]);

        $headers = [
            'Authorization' => "Bearer $token"
        ];

        $this->json('delete', 'api/domain/' . $domain->id, [], $headers)
            ->assertStatus(204);
    }

    /**
     * Delete An Exist Domain that not related to logged in users
     *
     * @return void
     */
    public function testDeleteDomainNotRelated()
    {
        //Simulating : User login
        $user = factory(User::class)->create(['email' => 'user@test.com']);
        $token = $user->generateApiToken();

        //create a domain
        $domain = factory(Domain::class)->create(['domain' => 'https://test.com']);

        $headers = [
            'Accept' => "application/json",
            'Content-Type' => "application/json",
            'Authorization' => "Bearer $token"
        ];

        $this->json('delete', 'api/domain/' . $domain->id, [], $headers)
            ->assertStatus(404);
    }


    /**
     * Delete An Exist Domain With Invalid Token
     *
     * @return void
     */
    public function testDeleteDomainWithIvalidToken()
    {
        //Invalid Token
        $token = Str::random(50);

        //create a domain
        $domain = factory(Domain::class)->create(['domain' => 'https://test.com']);

        $headers = [
            'Accept' => "application/json",
            'Content-Type' => "application/json",
            'Authorization' => "Bearer $token"
        ];

        $this->json('delete', 'api/domain/' . $domain->id, [], $headers)
            ->assertStatus(401);
    }
}
