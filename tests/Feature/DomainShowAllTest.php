<?php

namespace Tests\Feature;

use App\Domain;
use App\DomainUser;
use App\User;
use Tests\TestCase;

class DomainShowAllTest extends TestCase
{
    /**
     * Show All Domains That Related To Logged in User
     *
     * @return void
     */
    public function testAllDomainsRelatedToUser()
    {
        //Simulating : User login
        $user = factory(User::class)->create(['email' => 'user@test.com']);
        $token = $user->generateApiToken();

        //create a domain
        $domain = factory(Domain::class)->create();

        //Related Domain and User together
        $domainUser = factory(DomainUser::class)->create([
           'user_id' => $user->id,
           'domain_id' => $domain->id,
        ]);

        $headers = [
            'Accept' => "application/json",
            'Content-Type' => "application/json",
            'Authorization' => "Bearer $token"
        ];

        $this->json('get', '/api/domain/', [], $headers)
            ->assertStatus(200);

        $domainUserCount = DomainUser::where('user_id',$user->id)->count();

        $this->assertEquals(1, $domainUserCount);
    }
}
