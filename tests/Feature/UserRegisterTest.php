<?php

namespace Tests\Feature;

use Tests\TestCase;

class UserRegisterTest extends TestCase
{
    /**
     * Test Register Successfully
     *
     * @return void
     */
    public function testRegisterSuccessfully()
    {

        $headers = [
            'Accept' => "application/json",
            'Content-Type' => "application/json",
        ];

        $payLoad = [
            'name' => 'HRMtest',
            'email' => 'hrm@test.com',
            'password' => 'password',
            'password_confirmation' => 'password'
        ];

        $this->json('post', 'api/register', $payLoad , $headers)
            ->assertStatus(201)
            ->assertJsonStructure([
                "data" => [
                    "name",
                    "email",
                    "updated_at",
                    "created_at",
                    "id",
                    "api_token"
                ]
            ]);

    }

    /**
     * Test Register Requirement
     * Name , Email , Password
     * @return void
     */
    public function testRegisterRequirement()
    {
        $headers = [
            'Accept' => "application/json",
            'Content-Type' => "application/json",
        ];

        $this->json('post', '/api/register' , [] , $headers)
            ->assertStatus(422)
            ->assertJsonStructure([
                "message",
                "errors" => [
                    "name" => [],
                    "email" => [],
                    "password" => []
                ]
            ]);
    }

    /**
     * Test Register Requirement
     * Password Confirmation
     * @return void
     */
    public function testRegisterRequirePasswordConfirmation()
    {
        $headers = [
            'Accept' => "application/json",
            'Content-Type' => "application/json",
        ];

        $payLoad = [
            'name' => 'HRMtest',
            'email' => 'hrm@test.com',
            'password' => 'password',
        ];

        $this->json('post', '/api/register', $payLoad , $headers)
            ->assertStatus(422)
            ->assertJsonStructure([
                "message",
                "errors" => [
                    "password" => []
                ]
            ]);
    }

}
