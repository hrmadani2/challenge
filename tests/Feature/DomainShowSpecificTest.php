<?php

namespace Tests\Feature;

use App\Domain;
use App\DomainUser;
use App\User;
use Tests\TestCase;

class DomainShowSpecificTest extends TestCase
{
    /**
     * Show The Specific Domain That Related To Logged in User
     *
     * @return void
     */
    public function testShowSpecificDomain()
    {
        //Simulating : User login
        $user = factory(User::class)->create(['email' => 'user@test.com']);
        $token = $user->generateApiToken();

        //create a domain
        $domain = factory(Domain::class)->create(['domain' => 'https://testShowSpecificDomain.com']);

        //Join Domain and User together
        $domainUser = factory(DomainUser::class)->create([
            'user_id' => $user->id,
            'domain_id' => $domain->id,
        ]);

        $headers = [
            'Accept' => "application/json",
            'Content-Type' => "application/json",
            'Authorization' => "Bearer $token"
        ];

        $this->json('get', '/api/domain/' . $domain->id, [], $headers)
            ->assertStatus(200)
            ->assertJsonStructure([
                "data" => [
                    "id",
                    "domain",
                    "created_at",
                    "updated_at",
                    "confirmation" =>[
                        "domain_id",
                        "user_id",
                        "confirmed_at",
                        "confirmation_code"
                    ]
                ]
            ])->assertJson([
            'data' => [
                'id' => $domain->id,
                'domain' => $domain->domain
            ]
        ]);
    }

    /**
     * Doesn't Show The Specific Domain That not Related To Logged in User
     *
     * @return void
     */
    public function testNotShowSpecificDomain()
    {
        //Simulating : User login
        $user = factory(User::class)->create(['email' => 'user@testNotShowSpecificDomain.com']);
        $token = $user->generateApiToken();

        //create a domain
        $domain = factory(Domain::class)->create(['domain' => 'https://testNotShowSpecificDomain.com']);


        $headers = [
            'Accept' => "application/json",
            'Content-Type' => "application/json",
            'Authorization' => "Bearer $token"
        ];

        $this->json('get', '/api/domain/' . $domain->id, [], $headers)
            ->assertStatus(404)
            ->assertJson([
                "data"=> "Resource not found"
            ]);
    }
}
