<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
Route::group(['middleware' => 'auth:api'], function () {

    //Add URLs that related to Domain
    Route::prefix('domain')->group(function () {

        //Retrieve All Domains
        //Feature Test => OK
        Route::get('/', 'API\v1\DomainController@index');

        //Retrieve A Specific Domain
        //Feature Test (2)=> OK
        Route::get('/{domain}', 'API\v1\DomainController@show');

        //Save A New Domain
        //Feature Test (1) => OK
        Route::put('/', 'API\v1\DomainController@store');

        //Delete A Specific Domain
        //Feature Test (3)=> OK
        Route::delete('/{domain}', 'API\v1\DomainController@delete');

        //Check Confirmation of Domain
        //This version only support the text file in destination
        //But this feature can develop in other ways
        Route::get('confirmation/{domain}/{type?}', 'API\v1\ConfirmController@ConfirmDomain');
    });

    //User Logout
    //Feature Test => OK
    Route::get('logout', 'Auth\LoginController@logout');
});

//User Registration
//Feature Test (3) => OK
Route::post('register', 'Auth\RegisterController@register');

//User Login
//Feature Test (3)=> OK
Route::post('login', 'Auth\LoginController@login');